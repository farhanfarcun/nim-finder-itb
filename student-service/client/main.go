package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	studentPB "gitlab.com/farhanfarcun/nim-finder-itb/pb"
	"google.golang.org/grpc"
)

func initStudentServiceClient(port string) studentPB.StudentServiceClient {
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return studentPB.NewStudentServiceClient(conn)
}

func main() {
	client := initStudentServiceClient(":9000")
	res, err := client.GetStudentsByID(context.Background(), &studentPB.FindyByStudentID{Id: 13517001})
	if err != nil {
		log.Fatal(err)
	}
	for _, v := range res.Student {
		fmt.Println(v)
	}
}
