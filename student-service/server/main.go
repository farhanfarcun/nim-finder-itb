package main

import (
	"net"

	log "github.com/sirupsen/logrus"
	studentPB "gitlab.com/farhanfarcun/nim-finder-itb/pb"
	"gitlab.com/farhanfarcun/nim-finder-itb/repository/inmem"
	"gitlab.com/farhanfarcun/nim-finder-itb/repository/inmem/storage"
	"gitlab.com/farhanfarcun/nim-finder-itb/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	grpc := grpc.NewServer()
	reflection.Register(grpc)

	server := service.NewStudentService()
	storage := storage.NewStudentStorage()
	repository := inmem.NewStudentRepoInMemory(storage)
	server.RegisterStudentRepository(repository)
	studentPB.RegisterStudentServiceServer(grpc, server)

	log.Println("Starting grpc server at", 9000)
	listen, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("Could not listen to %s: %v", ":9000", err)
	}
	log.Fatal(grpc.Serve(listen))
}
