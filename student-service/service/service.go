package service

import (
	"context"
	"log"

	studentPB "gitlab.com/farhanfarcun/nim-finder-itb/pb"
	"gitlab.com/farhanfarcun/nim-finder-itb/repository"
	"gitlab.com/farhanfarcun/nim-finder-itb/repository/model"
)

// Service :nodoc:
type Service struct {
	studentRepo repository.StudentRepository
}

// NewStudentService :nodoc:
func NewStudentService() *Service {
	return new(Service)
}

// RegisterStudentRepository :nodoc:
func (s *Service) RegisterStudentRepository(r repository.StudentRepository) {
	s.studentRepo = r
}

// GetStudents is to get all students
func (s *Service) GetStudents(ctx context.Context, param *studentPB.Empty) (*studentPB.Students, error) {
	res := s.studentRepo.GetStudents()
	if res.Error != nil {
		return nil, res.Error
	}
	var result studentPB.Students
	for _, v := range res.Result.([]model.Student) {
		result.Student = append(result.Student, v.ToProto())
	}
	log.Println("GetStudents Query")
	return &result, nil
}

// GetStudentsByName is to  get all student by name
func (s *Service) GetStudentsByName(ctx context.Context, param *studentPB.FindyByName) (*studentPB.Students, error) {
	res := s.studentRepo.GetStudentsByName(param.Name)
	if res.Error != nil {
		return nil, res.Error
	}
	var result studentPB.Students
	for _, v := range res.Result.([]model.Student) {
		result.Student = append(result.Student, v.ToProto())
	}
	log.Println("GetStudentsByName Query")
	return &result, nil
}

// GetStudentsByMajor is to get all students by major
func (s *Service) GetStudentsByMajor(ctx context.Context, param *studentPB.FindyByMajor) (*studentPB.Students, error) {
	res := s.studentRepo.GetStudentsByMajor(param.Major)
	if res.Error != nil {
		return nil, res.Error
	}
	var result studentPB.Students
	for _, v := range res.Result.([]model.Student) {
		result.Student = append(result.Student, v.ToProto())
	}
	log.Println("GetStudentsByMajor Query")
	return &result, nil
}

// GetStudentsByID is to get student by id
func (s *Service) GetStudentsByID(ctx context.Context, param *studentPB.FindyByStudentID) (*studentPB.Students, error) {
	res := s.studentRepo.GetStudentsByID(param.Id)
	if res.Error != nil {
		return nil, res.Error
	}
	var result studentPB.Students
	for _, v := range res.Result.([]model.Student) {
		result.Student = append(result.Student, v.ToProto())
	}
	log.Println("GetStudentsByID Query")
	return &result, nil
}
