package inmem

import (
	"errors"
	"strings"

	"gitlab.com/farhanfarcun/nim-finder-itb/repository"
	"gitlab.com/farhanfarcun/nim-finder-itb/repository/inmem/storage"
	"gitlab.com/farhanfarcun/nim-finder-itb/repository/model"
)

// StudentRepoInMemory is Student Repo implementation in memory
type StudentRepoInMemory struct {
	Storage *storage.StudentStorage
}

var errQueryNotFound = errors.New("Query Not Found")

// NewStudentRepoInMemory is constructor
func NewStudentRepoInMemory(storage *storage.StudentStorage) repository.StudentRepository {
	return &StudentRepoInMemory{
		Storage: storage,
	}
}

// GetStudents is to get all students
func (sr *StudentRepoInMemory) GetStudents() repository.QueryResult {
	var result repository.QueryResult
	result.Result = sr.Storage.StudentArray
	return result
}

// GetStudentsByName is to get students with name
func (sr *StudentRepoInMemory) GetStudentsByName(Name string) repository.QueryResult {
	var result repository.QueryResult
	var array []model.Student
	for i, v := range sr.Storage.StudentArray {
		if strings.Contains(strings.ToLower(v.Name), strings.ToLower(Name)) {
			array = append(array, sr.Storage.StudentArray[i])
		}
	}
	if len(array) == 0 {
		result.Error = errQueryNotFound
	}
	result.Result = array
	return result
}

// GetStudentsByMajor is to get students with major
func (sr *StudentRepoInMemory) GetStudentsByMajor(major string) repository.QueryResult {
	var result repository.QueryResult
	var array []model.Student
	for i, v := range sr.Storage.StudentArray {
		if strings.Contains(strings.ToLower(v.Major), strings.ToLower(major)) {
			array = append(array, sr.Storage.StudentArray[i])
		}
	}
	if len(array) == 0 {
		result.Error = errQueryNotFound
	}
	result.Result = array
	return result
}

// GetStudentsByID is to get studentes with ID
func (sr *StudentRepoInMemory) GetStudentsByID(StudentID int32) repository.QueryResult {
	var result repository.QueryResult
	var array []model.Student
	for i, v := range sr.Storage.StudentArray {
		if v.StudentID == StudentID || v.StudentMajorID == StudentID {
			array = append(array, sr.Storage.StudentArray[i])
		}
	}
	if len(array) == 0 {
		result.Error = errQueryNotFound
	}
	result.Result = array
	return result
}
