package storage

import (
	"fmt"
	"strconv"

	"gitlab.com/farhanfarcun/nim-finder-itb/repository/model"
)

// StudentStorage is storage for student
type StudentStorage struct {
	StudentArray []model.Student
}

// NewStudentStorage is Inmemory storage
func NewStudentStorage() *StudentStorage {
	var students StudentStorage
	for i := 0; i < 10; i++ {
		nim := "1651700" + fmt.Sprint(i)
		nimJ := "1351700" + fmt.Sprint(i)
		name := "nama1 ke- " + fmt.Sprint(i)

		Nim, err := strconv.Atoi(nim)
		if err != nil {
			Nim = 0
		}
		NimJ, err := strconv.Atoi(nimJ)
		if err != nil {
			NimJ = 0
		}
		Nims := int32(Nim)
		NimsJ := int32(NimJ)
		student := model.Student{
			StudentID:      Nims,
			StudentMajorID: NimsJ,
			Name:           name,
			Major:          "Teknik Informatika",
		}
		students.StudentArray = append(students.StudentArray, student)
	}
	for i := 0; i < 10; i++ {
		nim := "1651700" + fmt.Sprint(i)
		nimJ := "1321700" + fmt.Sprint(i)
		name := "nama2 ke- " + fmt.Sprint(i)

		Nim, err := strconv.Atoi(nim)
		if err != nil {
			Nim = 0
		}
		NimJ, err := strconv.Atoi(nimJ)
		if err != nil {
			NimJ = 0
		}
		Nims := int32(Nim)
		NimsJ := int32(NimJ)
		student := model.Student{
			StudentID:      Nims,
			StudentMajorID: NimsJ,
			Name:           name,
			Major:          "Teknik Elektro",
		}
		students.StudentArray = append(students.StudentArray, student)
	}
	for i := 0; i < 10; i++ {
		nim := "1651700" + fmt.Sprint(i)
		nimJ := "1821700" + fmt.Sprint(i)
		name := "nama2 ke- " + fmt.Sprint(i)

		Nim, err := strconv.Atoi(nim)
		if err != nil {
			Nim = 0
		}
		NimJ, err := strconv.Atoi(nimJ)
		if err != nil {
			NimJ = 0
		}
		Nims := int32(Nim)
		NimsJ := int32(NimJ)
		student := model.Student{
			StudentID:      Nims,
			StudentMajorID: NimsJ,
			Name:           name,
			Major:          "Sistem Teknologi dan Informasi",
		}
		students.StudentArray = append(students.StudentArray, student)
	}
	for i := 0; i < 10; i++ {
		nim := "1641700" + fmt.Sprint(i)
		nimJ := "1231700" + fmt.Sprint(i)
		name := "nama2 ke- " + fmt.Sprint(i)

		Nim, err := strconv.Atoi(nim)
		if err != nil {
			Nim = 0
		}
		NimJ, err := strconv.Atoi(nimJ)
		if err != nil {
			NimJ = 0
		}
		Nims := int32(Nim)
		NimsJ := int32(NimJ)
		student := model.Student{
			StudentID:      Nims,
			StudentMajorID: NimsJ,
			Name:           name,
			Major:          "Teknik Geofisika",
		}
		students.StudentArray = append(students.StudentArray, student)
	}
	return &students
}
