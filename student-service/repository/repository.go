package repository

// QueryResult is struct to contain query result
type QueryResult struct {
	Result interface{}
	Error  error
}

type (
	// StudentRepository is interface for querying student
	StudentRepository interface {
		GetStudents() QueryResult
		GetStudentsByName(Name string) QueryResult
		GetStudentsByMajor(major string) QueryResult
		GetStudentsByID(StudentID int32) QueryResult
	}
)
