package model

import (
	studentPB "gitlab.com/farhanfarcun/nim-finder-itb/pb"
)

// Student is struct to contain
type Student struct {
	StudentID      int32
	StudentMajorID int32
	Name           string
	Major          string
}

// ToProto :nodoc:
func (s *Student) ToProto() *studentPB.Student {
	return &studentPB.Student{
		StudentId:      s.StudentID,
		StudentMajorId: s.StudentMajorID,
		Name:           s.Name,
		Major:          s.Major,
	}
}
