module gitlab.com/farhanfarcun/nim-finder-itb

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/sirupsen/logrus v1.4.2
	google.golang.org/grpc v1.23.0
)
