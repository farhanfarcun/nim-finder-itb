// Code generated by protoc-gen-go. DO NOT EDIT.
// source: pb/student_service.proto

package student

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Empty used when an RPC doesn't need to return any message
type Empty struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Empty) Reset()         { *m = Empty{} }
func (m *Empty) String() string { return proto.CompactTextString(m) }
func (*Empty) ProtoMessage()    {}
func (*Empty) Descriptor() ([]byte, []int) {
	return fileDescriptor_5759fbee4709651d, []int{0}
}

func (m *Empty) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Empty.Unmarshal(m, b)
}
func (m *Empty) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Empty.Marshal(b, m, deterministic)
}
func (m *Empty) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Empty.Merge(m, src)
}
func (m *Empty) XXX_Size() int {
	return xxx_messageInfo_Empty.Size(m)
}
func (m *Empty) XXX_DiscardUnknown() {
	xxx_messageInfo_Empty.DiscardUnknown(m)
}

var xxx_messageInfo_Empty proto.InternalMessageInfo

// FindyByStudentID :nodoc:
type FindyByStudentID struct {
	Id                   int32    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *FindyByStudentID) Reset()         { *m = FindyByStudentID{} }
func (m *FindyByStudentID) String() string { return proto.CompactTextString(m) }
func (*FindyByStudentID) ProtoMessage()    {}
func (*FindyByStudentID) Descriptor() ([]byte, []int) {
	return fileDescriptor_5759fbee4709651d, []int{1}
}

func (m *FindyByStudentID) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FindyByStudentID.Unmarshal(m, b)
}
func (m *FindyByStudentID) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FindyByStudentID.Marshal(b, m, deterministic)
}
func (m *FindyByStudentID) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FindyByStudentID.Merge(m, src)
}
func (m *FindyByStudentID) XXX_Size() int {
	return xxx_messageInfo_FindyByStudentID.Size(m)
}
func (m *FindyByStudentID) XXX_DiscardUnknown() {
	xxx_messageInfo_FindyByStudentID.DiscardUnknown(m)
}

var xxx_messageInfo_FindyByStudentID proto.InternalMessageInfo

func (m *FindyByStudentID) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

// FindyByMajor :nodoc:
type FindyByMajor struct {
	Major                string   `protobuf:"bytes,1,opt,name=major,proto3" json:"major,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *FindyByMajor) Reset()         { *m = FindyByMajor{} }
func (m *FindyByMajor) String() string { return proto.CompactTextString(m) }
func (*FindyByMajor) ProtoMessage()    {}
func (*FindyByMajor) Descriptor() ([]byte, []int) {
	return fileDescriptor_5759fbee4709651d, []int{2}
}

func (m *FindyByMajor) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FindyByMajor.Unmarshal(m, b)
}
func (m *FindyByMajor) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FindyByMajor.Marshal(b, m, deterministic)
}
func (m *FindyByMajor) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FindyByMajor.Merge(m, src)
}
func (m *FindyByMajor) XXX_Size() int {
	return xxx_messageInfo_FindyByMajor.Size(m)
}
func (m *FindyByMajor) XXX_DiscardUnknown() {
	xxx_messageInfo_FindyByMajor.DiscardUnknown(m)
}

var xxx_messageInfo_FindyByMajor proto.InternalMessageInfo

func (m *FindyByMajor) GetMajor() string {
	if m != nil {
		return m.Major
	}
	return ""
}

// FindyByName :nodoc:
type FindyByName struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *FindyByName) Reset()         { *m = FindyByName{} }
func (m *FindyByName) String() string { return proto.CompactTextString(m) }
func (*FindyByName) ProtoMessage()    {}
func (*FindyByName) Descriptor() ([]byte, []int) {
	return fileDescriptor_5759fbee4709651d, []int{3}
}

func (m *FindyByName) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FindyByName.Unmarshal(m, b)
}
func (m *FindyByName) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FindyByName.Marshal(b, m, deterministic)
}
func (m *FindyByName) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FindyByName.Merge(m, src)
}
func (m *FindyByName) XXX_Size() int {
	return xxx_messageInfo_FindyByName.Size(m)
}
func (m *FindyByName) XXX_DiscardUnknown() {
	xxx_messageInfo_FindyByName.DiscardUnknown(m)
}

var xxx_messageInfo_FindyByName proto.InternalMessageInfo

func (m *FindyByName) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func init() {
	proto.RegisterType((*Empty)(nil), "pb.student.Empty")
	proto.RegisterType((*FindyByStudentID)(nil), "pb.student.FindyByStudentID")
	proto.RegisterType((*FindyByMajor)(nil), "pb.student.FindyByMajor")
	proto.RegisterType((*FindyByName)(nil), "pb.student.FindyByName")
}

func init() { proto.RegisterFile("pb/student_service.proto", fileDescriptor_5759fbee4709651d) }

var fileDescriptor_5759fbee4709651d = []byte{
	// 245 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x28, 0x48, 0xd2, 0x2f,
	0x2e, 0x29, 0x4d, 0x49, 0xcd, 0x2b, 0x89, 0x2f, 0x4e, 0x2d, 0x2a, 0xcb, 0x4c, 0x4e, 0xd5, 0x2b,
	0x28, 0xca, 0x2f, 0xc9, 0x17, 0xe2, 0x2a, 0x48, 0xd2, 0x83, 0xca, 0x48, 0x09, 0x20, 0x54, 0x41,
	0x64, 0x95, 0xd8, 0xb9, 0x58, 0x5d, 0x73, 0x0b, 0x4a, 0x2a, 0x95, 0x94, 0xb8, 0x04, 0xdc, 0x32,
	0xf3, 0x52, 0x2a, 0x9d, 0x2a, 0x83, 0x21, 0x0a, 0x3c, 0x5d, 0x84, 0xf8, 0xb8, 0x98, 0x32, 0x53,
	0x24, 0x18, 0x15, 0x18, 0x35, 0x58, 0x83, 0x98, 0x32, 0x53, 0x94, 0x54, 0xb8, 0x78, 0xa0, 0x6a,
	0x7c, 0x13, 0xb3, 0xf2, 0x8b, 0x84, 0x44, 0xb8, 0x58, 0x73, 0x41, 0x0c, 0xb0, 0x12, 0xce, 0x20,
	0x08, 0x47, 0x49, 0x91, 0x8b, 0x1b, 0xaa, 0xca, 0x2f, 0x31, 0x37, 0x55, 0x48, 0x88, 0x8b, 0x25,
	0x2f, 0x31, 0x37, 0x15, 0xaa, 0x06, 0xcc, 0x36, 0x5a, 0xc8, 0xc4, 0xc5, 0x07, 0xb5, 0x26, 0x18,
	0xe2, 0x58, 0x21, 0x0b, 0x2e, 0x6e, 0xf7, 0xd4, 0x12, 0xa8, 0x60, 0xb1, 0x90, 0xa0, 0x1e, 0xc2,
	0xd9, 0x7a, 0x60, 0x17, 0x4a, 0x89, 0x20, 0x0b, 0xc1, 0x14, 0x2a, 0x31, 0x08, 0xb9, 0x70, 0x09,
	0x22, 0xe9, 0x84, 0xda, 0x2a, 0x8e, 0xac, 0x18, 0xc9, 0x39, 0x38, 0x4d, 0x71, 0xe3, 0x12, 0x42,
	0x31, 0x05, 0xe2, 0x43, 0x09, 0x2c, 0xc6, 0x80, 0x65, 0x70, 0x9a, 0xe3, 0xce, 0xc5, 0x8f, 0x62,
	0x8e, 0xa7, 0x8b, 0x90, 0x0c, 0x16, 0x43, 0xe0, 0x81, 0x8c, 0xcb, 0x20, 0x27, 0xce, 0x28, 0x76,
	0xa8, 0x68, 0x12, 0x1b, 0x38, 0xae, 0x8c, 0x01, 0x01, 0x00, 0x00, 0xff, 0xff, 0xd1, 0x1f, 0x8e,
	0x0b, 0xe5, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// StudentServiceClient is the client API for StudentService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type StudentServiceClient interface {
	GetStudents(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*Students, error)
	GetStudentsByName(ctx context.Context, in *FindyByName, opts ...grpc.CallOption) (*Students, error)
	GetStudentsByMajor(ctx context.Context, in *FindyByMajor, opts ...grpc.CallOption) (*Students, error)
	GetStudentsByID(ctx context.Context, in *FindyByStudentID, opts ...grpc.CallOption) (*Students, error)
}

type studentServiceClient struct {
	cc *grpc.ClientConn
}

func NewStudentServiceClient(cc *grpc.ClientConn) StudentServiceClient {
	return &studentServiceClient{cc}
}

func (c *studentServiceClient) GetStudents(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*Students, error) {
	out := new(Students)
	err := c.cc.Invoke(ctx, "/pb.student.StudentService/GetStudents", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *studentServiceClient) GetStudentsByName(ctx context.Context, in *FindyByName, opts ...grpc.CallOption) (*Students, error) {
	out := new(Students)
	err := c.cc.Invoke(ctx, "/pb.student.StudentService/GetStudentsByName", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *studentServiceClient) GetStudentsByMajor(ctx context.Context, in *FindyByMajor, opts ...grpc.CallOption) (*Students, error) {
	out := new(Students)
	err := c.cc.Invoke(ctx, "/pb.student.StudentService/GetStudentsByMajor", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *studentServiceClient) GetStudentsByID(ctx context.Context, in *FindyByStudentID, opts ...grpc.CallOption) (*Students, error) {
	out := new(Students)
	err := c.cc.Invoke(ctx, "/pb.student.StudentService/GetStudentsByID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// StudentServiceServer is the server API for StudentService service.
type StudentServiceServer interface {
	GetStudents(context.Context, *Empty) (*Students, error)
	GetStudentsByName(context.Context, *FindyByName) (*Students, error)
	GetStudentsByMajor(context.Context, *FindyByMajor) (*Students, error)
	GetStudentsByID(context.Context, *FindyByStudentID) (*Students, error)
}

// UnimplementedStudentServiceServer can be embedded to have forward compatible implementations.
type UnimplementedStudentServiceServer struct {
}

func (*UnimplementedStudentServiceServer) GetStudents(ctx context.Context, req *Empty) (*Students, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetStudents not implemented")
}
func (*UnimplementedStudentServiceServer) GetStudentsByName(ctx context.Context, req *FindyByName) (*Students, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetStudentsByName not implemented")
}
func (*UnimplementedStudentServiceServer) GetStudentsByMajor(ctx context.Context, req *FindyByMajor) (*Students, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetStudentsByMajor not implemented")
}
func (*UnimplementedStudentServiceServer) GetStudentsByID(ctx context.Context, req *FindyByStudentID) (*Students, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetStudentsByID not implemented")
}

func RegisterStudentServiceServer(s *grpc.Server, srv StudentServiceServer) {
	s.RegisterService(&_StudentService_serviceDesc, srv)
}

func _StudentService_GetStudents_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(StudentServiceServer).GetStudents(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pb.student.StudentService/GetStudents",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(StudentServiceServer).GetStudents(ctx, req.(*Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _StudentService_GetStudentsByName_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FindyByName)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(StudentServiceServer).GetStudentsByName(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pb.student.StudentService/GetStudentsByName",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(StudentServiceServer).GetStudentsByName(ctx, req.(*FindyByName))
	}
	return interceptor(ctx, in, info, handler)
}

func _StudentService_GetStudentsByMajor_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FindyByMajor)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(StudentServiceServer).GetStudentsByMajor(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pb.student.StudentService/GetStudentsByMajor",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(StudentServiceServer).GetStudentsByMajor(ctx, req.(*FindyByMajor))
	}
	return interceptor(ctx, in, info, handler)
}

func _StudentService_GetStudentsByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FindyByStudentID)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(StudentServiceServer).GetStudentsByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pb.student.StudentService/GetStudentsByID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(StudentServiceServer).GetStudentsByID(ctx, req.(*FindyByStudentID))
	}
	return interceptor(ctx, in, info, handler)
}

var _StudentService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "pb.student.StudentService",
	HandlerType: (*StudentServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetStudents",
			Handler:    _StudentService_GetStudents_Handler,
		},
		{
			MethodName: "GetStudentsByName",
			Handler:    _StudentService_GetStudentsByName_Handler,
		},
		{
			MethodName: "GetStudentsByMajor",
			Handler:    _StudentService_GetStudentsByMajor_Handler,
		},
		{
			MethodName: "GetStudentsByID",
			Handler:    _StudentService_GetStudentsByID_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "pb/student_service.proto",
}
