module gitlab.com/farhanfarcun/nim-finder-itb/gql

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.3.2
	github.com/graph-gophers/graphql-go v0.0.0-20190724201507-010347b5f9e6
	github.com/graphql-go/graphql v0.7.8
	github.com/graphql-go/handler v0.2.3
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	google.golang.org/grpc v1.23.0
)
