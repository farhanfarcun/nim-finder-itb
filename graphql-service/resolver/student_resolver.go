package resolver

import (
	studentPB "gitlab.com/farhanfarcun/nim-finder-itb/gql/pb"
)

// Student struct
type Student struct {
	student studentPB.Student
}

// StudentResolver :nodoc:
type StudentResolver struct {
	m Student
}

// Name :nodoc:
func (s *StudentResolver) Name() *string {
	return &s.m.student.Name
}

// Major :nodoc:
func (s *StudentResolver) Major() *string {
	return &s.m.student.Major
}

// StudentID :nodoc:
func (s *StudentResolver) StudentID() *int32 {
	return &s.m.student.StudentId
}

// StudentMajorID :nodoc:
func (s *StudentResolver) StudentMajorID() *int32 {
	return &s.m.student.StudentMajorId
}
