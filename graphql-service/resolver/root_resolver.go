package resolver

import (
	studentPB "gitlab.com/farhanfarcun/nim-finder-itb/gql/pb"
)

// Resolver :nodoc:
type Resolver struct {
	StudentServiceClient studentPB.StudentServiceClient
}
