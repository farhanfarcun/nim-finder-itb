package resolver

import (
	"context"

	studentPB "gitlab.com/farhanfarcun/nim-finder-itb/gql/pb"
)

func (r *Resolver) resolveStudents(ctx context.Context, students *studentPB.Students) ([]*StudentResolver, error) {
	loaded := make([]*StudentResolver, 0)
	for _, a := range students.Student {
		loaded = append(loaded, &StudentResolver{
			m: Student{
				student: *a,
			},
		})
	}

	return loaded, nil
}

// GetStudents :nodoc:
func (r *Resolver) GetStudents(ctx context.Context) (*[]*StudentResolver, error) {
	req := &studentPB.Empty{}
	students, err := r.StudentServiceClient.GetStudents(ctx, req)
	if err != nil {
		return nil, err
	}

	result, err := r.resolveStudents(ctx, students)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetStudentsByID :nodoc:
func (r *Resolver) GetStudentsByID(ctx context.Context, param struct {
	ID int32
}) (*[]*StudentResolver, error) {
	req := &studentPB.FindyByStudentID{Id: param.ID}
	students, err := r.StudentServiceClient.GetStudentsByID(ctx, req)
	if err != nil {
		return nil, err
	}

	result, err := r.resolveStudents(ctx, students)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetStudentsByMajor :nodoc:
func (r *Resolver) GetStudentsByMajor(ctx context.Context, param struct {
	Major string
}) (*[]*StudentResolver, error) {
	req := &studentPB.FindyByMajor{Major: param.Major}
	students, err := r.StudentServiceClient.GetStudentsByMajor(ctx, req)
	if err != nil {
		return nil, err
	}

	result, err := r.resolveStudents(ctx, students)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetStudentsByName :nodoc:
func (r *Resolver) GetStudentsByName(ctx context.Context, param struct {
	Name string
}) (*[]*StudentResolver, error) {
	req := &studentPB.FindyByName{Name: param.Name}
	students, err := r.StudentServiceClient.GetStudentsByName(ctx, req)
	if err != nil {
		return nil, err
	}

	result, err := r.resolveStudents(ctx, students)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
