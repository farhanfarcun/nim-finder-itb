package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	m "gitlab.com/farhanfarcun/nim-finder-itb/gql/middleware"
	studentPB "gitlab.com/farhanfarcun/nim-finder-itb/gql/pb"
	"gitlab.com/farhanfarcun/nim-finder-itb/gql/resolver"
	"google.golang.org/grpc"
)

func initStudentServiceClient(port string) studentPB.StudentServiceClient {
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return studentPB.NewStudentServiceClient(conn)
}

// CurrentUser is const
var CurrentUser = "user"

// JwtSecretKey is jwt secret key
var JwtSecretKey = []byte("iuhoii9234kjnjksdhu234")

// Get schema helper
func getSchema(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func main() {
	studentClient := initStudentServiceClient("localhost:9000")
	s, err := getSchema("./schema/schema.gql")
	if err != nil {
		panic(err)
	}

	resolver := &resolver.Resolver{
		StudentServiceClient: studentClient,
	}
	schema := graphql.MustParseSchema(s, resolver)

	// Http server
	e := echo.New()

	// Initialize Echo Middleware
	e.Use(middleware.Recover())
	e.Use(m.HeaderNoCache)
	e.Use(m.LogrusMiddleware())
	e.Use(middleware.RequestID())
	// e.Use(AuthService())

	e.POST("/login", Authenticate)
	e.POST("/graphql", wrapHandler(&relay.Handler{Schema: schema}))

	// Start server
	e.Logger.Fatal(e.Start("localhost:9001"))
}

// Authenticate is to handle authentication
func Authenticate(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")

	// change this into author service
	if username != "foobar" || password != "password" {
		return c.JSON(http.StatusUnprocessableEntity, map[string]string{
			"message": "User is not found",
		})
	}

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = username
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString(JwtSecretKey)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}

func wrapHandler(h http.Handler) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Original Request
		originalRequest := c.Request()
		originalContext := originalRequest.Context()

		var user string

		authorizationHeader := c.Request().Header.Get("Authorization")
		if strings.Contains(authorizationHeader, "Bearer") {
			tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

			token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
				if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("Signing method invalid")
				} else if method != jwt.SigningMethodHS256 {
					return nil, fmt.Errorf("Signing method invalid")
				}

				return JwtSecretKey, nil
			})

			claims, _ := token.Claims.(jwt.MapClaims)

			user = claims["username"].(string)
		}

		req := originalRequest.WithContext(
			context.WithValue(originalContext, CurrentUser, user),
		)

		h.ServeHTTP(c.Response(), req)
		return nil
	}
}
